﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using UniversityManagment.DTOs;
using UniversityManagment.Model;
using UniversityManagment.Repository;

namespace UniversityManagment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : Controller
    {
        private IStudentRepo _studentRepo;
        private readonly IMapper _mapper;
        public StudentController(IStudentRepo studentRepo, IMapper mapper )
        {
            _studentRepo = studentRepo;
            _mapper = mapper;
        }
        [Route("getAll")]
        [HttpGet]
        public IActionResult GetAllStudents()
        {
            List<Student> students = _studentRepo.GetAllStudents();
            return Ok(students);
        }
        [Route("getById")]
        [HttpGet]
        public IActionResult GetStudent(int id)
        {
            Student student = _studentRepo.GetStudent(id);
            if (student != null)
            {
                return Ok(student);
            }
            else
            {
                return BadRequest("Dont have this Student");
            }
        }
        [Route("addStudent")]
        [HttpPost]
        public IActionResult AddStudent(StudentDTO studentDTO)
        {
            Student student = _mapper.Map<Student>(studentDTO);
            _studentRepo.AddStudent(student);
            return Ok();
        }
        [Route("updateStudent")]
        [HttpPut]
        public IActionResult UpdateStudent(StudentDTO studentDTO , int id)
        {
            Student student = _mapper.Map<Student>(studentDTO);
            bool check = _studentRepo.UpdateStudent(student , id);
            if (check)
            {
                return Ok("Succesfuly");
            }
            else
            {
                return BadRequest("DOnt have this Id Student");
            }

        }
    }
}