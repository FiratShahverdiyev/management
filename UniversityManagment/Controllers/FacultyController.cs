﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using UniversityManagment.Model;
using UniversityManagment.Repository;

namespace UniversityManagment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FacultyController : Controller
    {
        private readonly IMapper _mapper;
        private IFacultyRepo _facultyRepo;
        public FacultyController(IMapper mapper, IFacultyRepo facultyRepo)
        {
            _facultyRepo = facultyRepo;
            _mapper = mapper;
        }
        [Route("getFaculties")]
        [HttpGet]
        public IActionResult GetAllFaculties()
        {
            List<Faculty> faculties = _facultyRepo.GetAllFaculty();
            return Ok(faculties);
        }
        [Route("getFaculty")]
        [HttpGet]
        public IActionResult GetFaculty(int id)
        {
            Faculty faculty = _facultyRepo.GetFaculty(id);
            return Ok(faculty);
        }
    }
}