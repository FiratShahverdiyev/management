﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using UniversityManagment.DTOs;
using UniversityManagment.Model;
using UniversityManagment.Repository;

namespace UniversityManagment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomController : Controller
    {
        private IRoomRepo _roomRepo;
        private readonly IMapper _mapper;
        public RoomController(IRoomRepo roomRepo, IMapper mapper)
        {
            _mapper = mapper;
            _roomRepo = roomRepo;
        }
        [Route("getAll")]
        [HttpGet]
        public IActionResult GetAllStudents()
        {
            List<Room> rooms = _roomRepo.GetAllRooms();
            return Ok(rooms);
        }
        [Route("getById")]
        [HttpGet]
        public IActionResult GetStudent(int id)
        {
            Room room = _roomRepo.GetRoom(id);
            if (room != null)
            {
                return Ok(room);
            }
            else
            {
                return BadRequest("Dont have this Room");
            }
        }
        [Route("addRoom")]
        [HttpPost]
        public IActionResult AddStudent(RoomDTO roomDTO)
        {
            Room room = _mapper.Map<Room>(roomDTO);
            _roomRepo.AddRoom(room);
            return Ok();
        }
        [Route("updateRoom")]
        [HttpPut]
        public IActionResult UpdateStudent(RoomDTO roomDTO, int id)
        {
            Room room = _mapper.Map<Room>(roomDTO);
            bool check = _roomRepo.UpdateRoom(room, id);
            if (check)
            {
                return Ok("Succesfuly");
            }
            else
            {
                return BadRequest("DOnt have this Id Room");
            }

        }
    }
}