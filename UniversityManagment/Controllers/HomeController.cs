﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using UniversityManagment.DTOs;
using UniversityManagment.Model;
using UniversityManagment.Repository;

namespace UniversityManagment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : Controller
    {
        private IManagmentRepo _managmentRepo;
        private readonly IMapper _mapper;
        public HomeController(IManagmentRepo managmentRepo , IMapper mapper)
        {
            _managmentRepo = managmentRepo;
            _mapper = mapper;
        }
        [Route("GetAllGroupWithStudents")]
        [HttpGet]
        public IActionResult GetAll() // yaxsi deyil heleki .
        {
            List<Student> students = _managmentRepo.GetAllGroupWithStudents();
            return Ok(students);
        }
        [Route("GetGroupWithStudents")]
        [HttpGet]
        public IActionResult GetGroup(int id) 
        {
            List<Student> students = _managmentRepo.GetGroupWithStudents(id);
            return Ok(students);
        }
    }
}