﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using UniversityManagment.DTOs;
using UniversityManagment.Model;
using UniversityManagment.Repository;

namespace UniversityManagment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeacherController : Controller
    {
        private ITeacherRepo _teacherRepo;
        private readonly IMapper _mapper;
        public TeacherController(ITeacherRepo teacherRepo, IMapper mapper)
        {
            _teacherRepo = teacherRepo;
            _mapper = mapper;
        }
        [Route("getAll")]
        [HttpGet]
        public IActionResult GetAllStudents()
        {
            List<Teacher> teachers = _teacherRepo.GetAllTeachers();
            return Ok(teachers);
        }
        [Route("getById")]
        [HttpGet]
        public IActionResult GetStudent(int id)
        {
            Teacher teacher = _teacherRepo.GetTeacher(id);
            if (teacher != null)
            {
                return Ok(teacher);
            }
            else
            {
                return BadRequest("Dont have this Teacher");
            }
        }
        [Route("addTeacher")]
        [HttpPost]
        public IActionResult AddStudent(TeacherDTO teacherDTO)
        {
            Teacher student = _mapper.Map<Teacher>(teacherDTO);
            _teacherRepo.AddTeacher(student);
            return Ok();
        }
        [Route("updateTeacher")]
        [HttpPut]
        public IActionResult UpdateStudent(TeacherDTO teacherDTO, int id)
        {
            Teacher student = _mapper.Map<Teacher>(teacherDTO);
            bool check = _teacherRepo.UpdateTeacher(student, id);
            if (check)
            {
                return Ok("Succesfuly");
            }
            else
            {
                return BadRequest("DOnt have this Id Teacher");
            }

        }
    }
}