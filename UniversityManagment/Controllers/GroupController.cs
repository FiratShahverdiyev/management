using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using UniversityManagment.DTOs;
using UniversityManagment.Model;
using UniversityManagment.Repository;

namespace UniversityManagment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupController : Controller
    {
        private IGroupRepo _groupRepo;
        private readonly IMapper _mapper;
        public GroupController(IGroupRepo groupRepo, IMapper mapper)
        {
            _mapper = mapper;
            _groupRepo = groupRepo;
        }

        [Route("getAll")]
        [HttpGet]
        public IActionResult GetAllGroups()
        {
            List<Group> groups = _groupRepo.GetAllGroup();
            return Ok(groups);
        }
        [Route("getById")]
        [HttpGet]
        public IActionResult Get(int id)
        {
            Group groups = _groupRepo.GetGroup(id);
            if (groups != null)
            {
                return Ok(groups);
            }
            else
            {
                return BadRequest("Dont have this Group");
            }
        }
        [Route("addGroup")]
        [HttpPost]
        public IActionResult AddGroup(GroupDTO groupDTO)
        {
            Group group = _mapper.Map<Group>(groupDTO);
            _groupRepo.AddGroup(group);
            return Ok();
        }
        [Route("updateGroup")]
        [HttpPut]
        public IActionResult UpdateGroup(GroupDTO groupDTO, int id)
        {
            Group group = _mapper.Map<Group>(groupDTO);
            bool check = _groupRepo.UpdateGroup(group, id);
            if (check)
            {
                return Ok("Succesfuly");
            }
            else
            {
                return BadRequest("Dont have this Id Group");
            }

        }
    }
}