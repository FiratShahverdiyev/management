﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using UniversityManagment.Model;

namespace UniversityManagment.Repository
{
    public class TeacherRepo : ITeacherRepo
    {
        string connection = "Data source=DESKTOP-PMH3J5I\\DB1; Initial Catalog=UniversityManagment ; Integrated Security=true";
        List<Teacher> teachers = new List<Teacher>();
        public void AddTeacher(Teacher teacher)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                string query = "Insert Into Teacher (Firstname, Lastname) values" +
                    " (@Firstname, @Lastname)";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@Firstname", teacher.FirstName);
                command.Parameters.AddWithValue("@Lastname", teacher.LastName);
                command.ExecuteNonQuery();
                sqlConnection.Close();
            }
        }

        public List<Teacher> GetAllTeachers()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                teachers.Clear();
                string query = "Select * from Teacher";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Teacher teacher = new Teacher();
                    teacher.Id = Convert.ToInt32(dataReader["Id"]);
                    teacher.FirstName = dataReader["Firstname"].ToString();
                    teacher.LastName = dataReader["Lastname"].ToString();
                    teachers.Add(teacher);
                }
                dataReader.Close();
                sqlConnection.Close();
                return teachers;
            }
        }

        public Teacher GetTeacher(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                teachers.Clear();
                string query = "Select * from Teacher where Id=@Id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@Id", id);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    Teacher teacher = new Teacher();
                    teacher.Id = Convert.ToInt32(dataReader["Id"]);
                    teacher.FirstName = dataReader["Firstname"].ToString();
                    teacher.LastName = dataReader["Lastname"].ToString();
                    dataReader.Close();
                    sqlConnection.Close();
                    return teacher;
                }
                return null;
            }
        }

        public bool UpdateTeacher(Teacher teacher, int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                string query1 = "Select * from Teacher where Id=@Id";
                SqlCommand sqlCommand1 = new SqlCommand(query1, sqlConnection);
                sqlCommand1.Parameters.AddWithValue("Id", id);
                SqlDataReader dataReader = sqlCommand1.ExecuteReader();
                if (dataReader.Read())
                {
                    string query2 = "Update Student Set Firstname=@Firstname , Lastname=@Lastname" +
                        " where Id=@Id";
                    SqlCommand sqlCommand2 = new SqlCommand(query2, sqlConnection);
                    sqlCommand2.CommandType = CommandType.Text;
                    teacher.Id = Convert.ToInt32(dataReader["Id"]);
                    teacher.FirstName = dataReader["Firstname"].ToString();
                    teacher.LastName = dataReader["Lastname"].ToString();
                    dataReader.Close();
                    sqlCommand2.ExecuteNonQuery();
                    sqlConnection.Close();
                    return true;
                }
                else
                {
                    dataReader.Close();
                    sqlConnection.Close();
                    return false;
                }
            }
        }
    }
}