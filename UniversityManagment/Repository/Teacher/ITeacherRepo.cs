﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniversityManagment.Model;

namespace UniversityManagment.Repository
{
    public interface ITeacherRepo
    {
        public void AddTeacher(Teacher teacher);
        public bool UpdateTeacher(Teacher teacher, int id);
        public Teacher GetTeacher(int id);
        public List<Teacher> GetAllTeachers();
    }
}
