﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniversityManagment.Model;

namespace UniversityManagment.Repository
{
   public interface IManagmentRepo
    {
        public List<Student> GetAllGroupWithStudents();
        public List<Student> GetGroupWithStudents(int id);

    }
}
