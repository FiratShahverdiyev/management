﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using UniversityManagment.DTOs;
using UniversityManagment.Model;

namespace UniversityManagment.Repository
{
    public class ManagmentRepo : IManagmentRepo
    {
        string connection = "Data source=DESKTOP-PMH3J5I\\DB1; Initial Catalog=UniversityManagment ; Integrated Security=true";
        List<Student> students = new List<Student>();

        public List<Student> GetAllGroupWithStudents()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                students.Clear();
                string query = "Select Student.Id , Student.FirstName , Student.Lastname , Student.Faculty , Student.GroupName from Student " +
                    "Inner Join Groups on Student.GroupName = Groups.GroupName";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Student student = new Student();
                    student.Id = Convert.ToInt32(dataReader["Id"]);
                    student.FirstName = dataReader["Firstname"].ToString();
                    student.LastName = dataReader["Lastname"].ToString();
                    student.Faculty = dataReader["Faculty"].ToString();
                    student.GroupName = dataReader["GroupName"].ToString();
                    students.Add(student);
                }
                dataReader.Close();
                sqlConnection.Close();
                return students;

            }
        }

        public List<Student> GetGroupWithStudents(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                students.Clear();
                sqlConnection.Open();
                string query = "Select Student.Id , Student.FirstName , Student.Lastname , Student.Faculty , Student.GroupName from Student"+
                   " Inner Join Groups on Student.GroupName = Groups.GroupName where Groups.Id = @Id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@Id", id);
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Student student = new Student();
                    student.Id = Convert.ToInt32(dataReader["Id"]);
                    student.FirstName = dataReader["Firstname"].ToString();
                    student.LastName = dataReader["Lastname"].ToString();
                    student.Faculty = dataReader["Faculty"].ToString();
                    student.GroupName = dataReader["GroupName"].ToString();
                    students.Add(student);
                }
                dataReader.Close();
                sqlConnection.Close();
                return students;
            }
        }
    }
}
