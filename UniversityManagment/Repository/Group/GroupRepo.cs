﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using UniversityManagment.Model;

namespace UniversityManagment.Repository
{
    public class GroupRepo : IGroupRepo
    {
        string connection = "Data source=DESKTOP-PMH3J5I\\DB1; Initial Catalog=UniversityManagment ; Integrated Security=true";
        List<Group> groups = new List<Group>();
        public void AddGroup(Group group)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                string query = "Insert Into Groups (GroupName) values" +
                    " (@GroupName)";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@GroupName", group.GroupName);
                command.ExecuteNonQuery();
                sqlConnection.Close();
            }
        }

        public List<Group> GetAllGroup()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                groups.Clear();
                string query = "Select * from Groups";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Group group = new Group();
                    group.Id = Convert.ToInt32(dataReader["Id"]);
                    group.GroupName =  dataReader["GroupName"].ToString();
                    groups.Add(group);
                }
                dataReader.Close();
                sqlConnection.Close();
                return groups;
            }
        }

        public Group GetGroup(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                groups.Clear();
                string query = "Select * from Groups where Id=@Id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@Id", id);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    Group group = new Group();
                    group.Id = Convert.ToInt32(dataReader["Id"]);
                    group.GroupName = dataReader["GroupName"].ToString();
                    dataReader.Close();
                    sqlConnection.Close();
                    return group;
                }
                return null;
            }
        }

        public bool UpdateGroup(Group group, int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                string query1 = "Select * from Groups where Id=@Id";
                SqlCommand sqlCommand1 = new SqlCommand(query1, sqlConnection);
                sqlCommand1.Parameters.AddWithValue("Id", id);
                SqlDataReader dataReader = sqlCommand1.ExecuteReader();
                if (dataReader.Read())
                {
                    string query2 = "Update Groups Set GroupName=@GroupName where Id=@Id";
                    SqlCommand sqlCommand2 = new SqlCommand(query2, sqlConnection);
                    sqlCommand2.CommandType = CommandType.Text;
                    sqlCommand2.Parameters.AddWithValue("Id", id);
                    sqlCommand2.Parameters.AddWithValue("GroupName", group.GroupName);
                    dataReader.Close();
                    sqlCommand2.ExecuteNonQuery();
                    sqlConnection.Close();
                    return true;
                }
                else
                {
                    dataReader.Close();
                    sqlConnection.Close();
                    return false;
                }
            }
        }
    }
}
