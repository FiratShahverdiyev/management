﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniversityManagment.Model;
namespace UniversityManagment.Repository
{
   public interface IGroupRepo
    {
        public void AddGroup(Group group);
        public bool UpdateGroup(Group group, int id);
        public Group GetGroup(int id);
        public List<Group> GetAllGroup();
    }
}
