﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniversityManagment.Model;

namespace UniversityManagment.Repository
{
  public interface IRoomRepo
    {
        public void AddRoom(Room room);
        public bool UpdateRoom(Room room, int id);
        public Room GetRoom(int id);
        public List<Room> GetAllRooms();
    }
}
