﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using UniversityManagment.Model;

namespace UniversityManagment.Repository
{
    public class RoomRepo : IRoomRepo
    {
        string connection = "Data source=DESKTOP-PMH3J5I\\DB1; Initial Catalog=UniversityManagment ; Integrated Security=true";
        List<Room> rooms = new List<Room>();

        public void AddRoom(Room room)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                string query = "Insert Into Room (Number) values" +
                    " (@Number)";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@Number", room.Number);
                command.ExecuteNonQuery();
                sqlConnection.Close();
            }
        }

        public List<Room> GetAllRooms()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                rooms.Clear();
                string query = "Select * from Room";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Room room = new Room();
                    room.Id = Convert.ToInt32(dataReader["Id"]);
                    room.Number = Convert.ToInt32(dataReader["Number"]);
                    rooms.Add(room);
                }
                dataReader.Close();
                sqlConnection.Close();
                return rooms;
            }
        }

        public Room GetRoom(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                rooms.Clear();
                string query = "Select * from Room where Id=@Id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@Id", id);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    Room room = new Room();
                    room.Id = Convert.ToInt32(dataReader["Id"]);
                    room.Number = Convert.ToInt32(dataReader["Number"]);
                    dataReader.Close();
                    sqlConnection.Close();
                    return room;
                }
                return null;
            }
        }

        public bool UpdateRoom(Room room, int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                string query1 = "Select * from Room where Id=@Id";
                SqlCommand sqlCommand1 = new SqlCommand(query1, sqlConnection);
                sqlCommand1.Parameters.AddWithValue("Id", id);
                SqlDataReader dataReader = sqlCommand1.ExecuteReader();
                if (dataReader.Read())
                {
                    string query2 = "Update Room Set Number=@Number where Id=@Id";
                    SqlCommand sqlCommand2 = new SqlCommand(query2, sqlConnection);
                    sqlCommand2.CommandType = CommandType.Text;
                    sqlCommand2.Parameters.AddWithValue("Id", id);
                    sqlCommand2.Parameters.AddWithValue("Number", room.Number);
                    dataReader.Close();
                    sqlCommand2.ExecuteNonQuery();
                    sqlConnection.Close();
                    return true;
                }
                else
                {
                    dataReader.Close();
                    sqlConnection.Close();
                    return false;
                }
            }
        }
    }
}
