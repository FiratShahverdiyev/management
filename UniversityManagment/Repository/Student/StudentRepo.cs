﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using UniversityManagment.Model;

namespace UniversityManagment.Repository
{
    public class StudentRepo : IStudentRepo
    {
        string connection = "Data source=DESKTOP-PMH3J5I\\DB1; Initial Catalog=UniversityManagment ; Integrated Security=true";
        List<Student> students = new List<Student>();
        public void AddStudent(Student student)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                string query = "Insert Into Student (Firstname, Lastname, Faculty , GroupName) values" +
                    " (@Firstname, @Lastname, @Faculty , @GroupName)";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@Firstname", student.FirstName);
                command.Parameters.AddWithValue("@Lastname", student.LastName);
                command.Parameters.AddWithValue("@Faculty", student.Faculty);
                command.Parameters.AddWithValue("@GroupName", student.GroupName);
                command.ExecuteNonQuery();
                sqlConnection.Close();       
            }
        }

        public List<Student> GetAllStudents()
        {
          using(SqlConnection sqlConnection=new SqlConnection(connection))
            {
                sqlConnection.Open();
                students.Clear();
                string query = "Select * from Student";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Student student = new Student();
                    student.Id = Convert.ToInt32(dataReader["Id"]);
                    student.FirstName = dataReader["Firstname"].ToString();
                    student.LastName = dataReader["Lastname"].ToString();
                    student.Faculty = dataReader["Faculty"].ToString();
                    student.GroupName = dataReader["GroupName"].ToString();
                    students.Add(student);
                }
                dataReader.Close();
                sqlConnection.Close();
                return students;
            }
        }

        public Student GetStudent(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                students.Clear();
                string query = "Select * from Student where Id=@Id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@Id", id);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    Student student = new Student();
                    student.Id = Convert.ToInt32(dataReader["Id"]);
                    student.FirstName = dataReader["Firstname"].ToString();
                    student.LastName = dataReader["Lastname"].ToString();
                    student.Faculty = dataReader["Faculty"].ToString();
                    student.GroupName = dataReader["GroupName"].ToString();
                    dataReader.Close();
                    sqlConnection.Close();
                    return student;
                }
                return null;
            }

            }

        public bool UpdateStudent(Student student, int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                string query1 = "Select * from Student where Id=@Id";
                SqlCommand sqlCommand1 = new SqlCommand(query1, sqlConnection);
                sqlCommand1.Parameters.AddWithValue("Id", id);
                SqlDataReader dataReader = sqlCommand1.ExecuteReader();
                if (dataReader.Read())
                {
                    string query2 = "Update Student Set Firstname=@Firstname , Lastname=@Lastname" +
                        ", Faculty=@Faculty , GroupName=@GroupName where Id=@Id";
                    SqlCommand sqlCommand2 = new SqlCommand(query2, sqlConnection);
                    sqlCommand2.CommandType = CommandType.Text;
                    sqlCommand2.Parameters.AddWithValue("Id", id);
                    sqlCommand2.Parameters.AddWithValue("Firstname", student.FirstName);
                    sqlCommand2.Parameters.AddWithValue("Lastname", student.LastName);
                    sqlCommand2.Parameters.AddWithValue("Faculty", student.Faculty);
                    sqlCommand2.Parameters.AddWithValue("GroupName", student.GroupName);
                    dataReader.Close();
                    sqlCommand2.ExecuteNonQuery();
                    sqlConnection.Close();
                    return true;
                }
                else
                {
                    dataReader.Close();
                    sqlConnection.Close();
                    return false;
                }

            }
        }
    }
}
