﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniversityManagment.Model;

namespace UniversityManagment.Repository
{
    public interface IStudentRepo
    {
        public void AddStudent(Student student);
        public bool UpdateStudent(Student student, int id);
        public Student GetStudent(int id);
        public List<Student> GetAllStudents();

    }
}
