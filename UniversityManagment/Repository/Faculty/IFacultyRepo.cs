﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniversityManagment.Model;

namespace UniversityManagment.Repository
{
    public interface IFacultyRepo
    {
        public Faculty GetFaculty(int id);
        public List<Faculty> GetAllFaculty();
    }
}
