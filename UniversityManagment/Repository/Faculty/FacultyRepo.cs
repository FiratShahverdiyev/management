﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using UniversityManagment.Model;

namespace UniversityManagment.Repository
{
    public class FacultyRepo : IFacultyRepo
    {
        string connection = "Data source=DESKTOP-PMH3J5I\\DB1; Initial Catalog=UniversityManagment ; Integrated Security=true";
        List<Faculty> faculties = new List<Faculty>();
        public List<Faculty> GetAllFaculty()
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                faculties.Clear();
                string query = "Select * from Faculty";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                SqlDataReader dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    Faculty faculty = new Faculty();
                    faculty.Id = Convert.ToInt32(dataReader["Id"]);
                    faculty.FacultyName = dataReader["FacultyName"].ToString();
                    faculties.Add(faculty);
                }
                dataReader.Close();
                sqlConnection.Close();
                return faculties;
            }
        }

        public Faculty GetFaculty(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlConnection.Open();
                string query = "Select * from Faculty where Id=@Id";
                SqlCommand command = new SqlCommand(query, sqlConnection);
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("@Id", id);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    Faculty faculty = new Faculty();
                    faculty.Id = Convert.ToInt32(dataReader["Id"]);
                    faculty.FacultyName = dataReader["FacultyName"].ToString();
                    dataReader.Close();
                    sqlConnection.Close();
                    return faculty;
                }
                return null;
            }
        }
    }
}
