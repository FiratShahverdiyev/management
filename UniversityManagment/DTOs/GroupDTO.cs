﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UniversityManagment.DTOs
{
    public class GroupDTO
    {
        public string GroupName { get; set; }
    }
}
