﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UniversityManagment.DTOs
{
    public class TeacherDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
