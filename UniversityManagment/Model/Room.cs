﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UniversityManagment.Model
{
    public class Room
    {
        public int Id { get; set; }
        public int Number { get; set; }
    }
}
